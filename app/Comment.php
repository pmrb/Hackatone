<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
  protected $fillable = ['name', 'company', 'title', 'content'];

  public function images()
  {
    return $this->hasMany('App\Image');
  }
}
