<?php

namespace App\Http\Controllers\Admin;

use App\Comment;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Image;
use App\Repositories\ImageRepository;
use App\Repositories\MessageRepository;
use Illuminate\Http\Request;
use Redis;

class AdminController extends Controller
{
    public function __construct(MessageRepository $messages, ImageRepository $images)
    {
        $this->middleware('auth');
        $this->messages = $messages;
        $this->images = $images;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $message = Comment::All();

        return view('admin.index', [
            'messages' => $message,
        ]);
    }

    public function decision()
    {
        return view('admin.index', ['messages' => Comment::All()]);
    }
}
