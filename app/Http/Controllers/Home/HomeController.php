<?php

namespace App\Http\Controllers\Home;

use App\Comment;
use App\Http\Controllers\Controller;
use App\Image;
use App\Repositories\ImageRepository;
use App\Repositories\MessageRepository;
use Illuminate\Http\Request;

class HomeController extends Controller
{
  protected $messages;
  protected $images;

  public function __construct(MessageRepository $messages, ImageRepository $images)
  {
    $this->messages = $messages;
    $this->images = $images;
  }

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    return view('home.home', [
      // TODO: only the ones approved
      'messages' => Comment::All()
    ]);
  }

  /**
   * Store Client
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function message(Request $request) {
    if ($request->isMethod('post')) {
      $this->validate($request, [
        'name'    => 'required',
        'company' => 'required',
        'title'   => 'required',
        'message' => 'required',
      ]);

      $data = $this->parseRequest($request);

      $message = $this->messages->create($data);

      $files = $request->file('file');

      if($files) {
        foreach ($files as $file) {
          $data = $this->parseAndMoveFileForImage($file, $message);

          $this->images->create($data);
        }
      }

      return redirect('/');

    } else {
      return view('messages.message');
    }
  }

  private function parseRequest($request) {
    $data = [
      'name'    => $request->input('name'),
      'company' => $request->input('company'),
      'title'   => $request->input('title'),
      'content' => $request->input('message')
    ];

    return $data;
  }

  private function parseAndMoveFileForImage($file, $message) {
    $name = time() . $file->getClientOriginalName();

    $file->move(Image::getPath(), $name);

    $data = [
      'comment_id' => $message->id,
      'path'       => Image::getPath() . $name,
      'size'       => $file->getClientSize(),
      'filename'   => $file->getClientOriginalName(),
      'mime'       => $file->getClientMimeType()
    ];

    return $data;
  }
}
