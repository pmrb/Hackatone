<?php
// Home
Route::get('/', 'Home\HomeController@index');
Route::get('/message', 'Home\HomeController@message');
Route::post('/message', 'Home\HomeController@message');
Route::post('/file', 'Home\HomeController@message');

// Authentication routes
Route::get('auth/login', 'Auth\AuthController@getLogin');
Route::post('auth/login', 'Auth\AuthController@postLogin');
Route::get('auth/logout', 'Auth\AuthController@getLogout');

// Registration routes
Route::get('auth/register', 'Auth\AuthController@getRegister');
Route::post('auth/register', 'Auth\AuthController@postRegister');

// Admin
Route::get('/admin', 'Admin\AdminController@index');
Route::post('/decision', 'Admin\AdminController@index');
