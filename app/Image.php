<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
  private static $path = 'images/';

  protected $fillable = ['comment_id', 'path', 'size', 'filename', 'mime'];

  public function comment() {
    return $this->belongsTo(Comment::class);
  }

  public static function getPath() {
    return self::$path;
  }
}
