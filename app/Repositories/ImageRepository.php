<?php

namespace App\Repositories;

use App\Image;
use App\Repositories\Contracts\RepositoryContract;

class ImageRepository implements RepositoryContract
{
    public function all($columns = array('*'))
    {
        return Image::get($columns);
    }

    public function paginate($perPage = 15, $columns = array('*')) {
        return Image::paginate($perPage, $columns);
    }

    public function create(array $data) {
        return Image::create($data);
    }

    public function update(array $data, $id) {
        return Image::where($attribute, '=', $id)->update($data);
    }

    public function delete($id) {
        return Image::destroy($id);
    }

    public function find($comment_id, $columns = array('*')) {
        return Image::where('comment_id', $comment_id)
                    ->orderBy('created_at', 'asc')
                    ->get();
    }

    public function findBy($field, $value, $columns = array('*')) {
        return Image::where($attribute, '=', $value)
                    ->first($columns);
    }
}