<?php

namespace App\Repositories;

use App\Comment;
use App\Repositories\Contracts\RepositoryContract;

class MessageRepository implements RepositoryContract
{
    public function all($columns = array('*'))
    {
        return Comment::get($columns);
    }

    public function paginate($perPage = 15, $columns = array('*')) {
        return Comment::paginate($perPage, $columns);
    }

    public function create(array $data) {
        return Comment::create($data);
    }

    public function update(array $data, $id) {
        return Comment::where($attribute, '=', $id)->update($data);
    }

    public function delete($id) {
        return Comment::destroy($id);
    }

    public function find($id, $columns = array('*')) {
        return Comment::where('id', $id)
                    ->orderBy('created_at', 'asc')
                    ->get();
    }

    public function findBy($field, $value, $columns = array('*')) {
        return Comment::where($attribute, '=', $value)
                    ->first($columns);
    }
}