<?php

use Illuminate\Database\Seeder;

class CommentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('comments')->insert([
        'client_id' => 1,
        'image_id' => 1,
        'title' => 'Test',
        'content' => 'This is a test',
      ]);
    }
}
