<?php

use Illuminate\Database\Seeder;

class ImageTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('images')->insert([
        'client_id' => 1,
        'title' => 'Avatar Image',
        'size' => 1024,
        'category' => 'Avatar',
        'filename' => 'pedro',
        'mime' => 'jpg',
      ]);
    }
}
