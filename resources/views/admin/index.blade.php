@extends('layouts.master')

@section('content')
  {!! Form::open(array('url' => 'foo/bar')) !!}
    @foreach ($messages as $message)
      {{ $message->title }}
      {{ $message->content }}
      {!! Form::label('content', 'Message') !!}
      {!! Form::checkbox('name', 'value', false) !!}
    @endforeach
  {!! Form::close() !!}
@endsection