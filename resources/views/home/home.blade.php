@extends('layouts.master')

@section('content')
    @if ($messages)
        @foreach ($messages as $message)
            <h3>{{ $message->title }}</h3>
            <p>{{ $message->content }}</p>
        @endforeach
    @endif

    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
@endsection