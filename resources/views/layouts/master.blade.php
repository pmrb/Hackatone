<!DOCTYPE html>
<html>
    <head>
        <title>Hackatone</title>
        <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">
        <link href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.2.0/dropzone.css" rel="stylesheet" type="text/css">
    </head>
    <body>
        <main>
            @yield('content')
        </main>

        <script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
        <script src="js/dropzone.js"></script>
        <script>
            Dropzone.options.messageForm = {
                uploadMultiple: true,
                maxFilesize: 2
            };
        </script>
    </body>
</html>