@extends('layouts.master')

@section('content')
    {!! Form::open(array('url' => '/message', 'method' => 'POST', 'class' => 'dropzone', 'id' => 'messageForm')) !!}
        {!! Form::label('name', 'Name') !!}
        {!! Form::text('name') !!}
        {!! Form::label('company', 'Company') !!}
        {!! Form::text('company') !!}
        {!! Form::label('title', 'Title') !!}
        {!! Form::text('title') !!}
        {!! Form::label('content', 'Message') !!}
        {!! Form::textArea('message') !!}
    {!! Form::close() !!}
@endsection